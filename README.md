# Deploy nolca-wed en AWS
A continuación se detalla el deploy de nolca-web en la cuenta nolca-nonprod.

> Nota: Si se desea instalar en otra cuenta se deben actualizar los datos.

### Sistema Operativo Windows
#### Pre Requesitos:
- Instalar AWS cli (https://awscli.amazonaws.com/AWSCLIV2.msi)
- Instalar AWS SAM cli (https://github.com/aws/aws-sam-cli/releases/latest/download/AWS_SAM_CLI_64_PY3.msi)
- Actualizar las credenciales AWS  https://d-90671546e0.awsapps.com/start#/

### Crear instancia EC2 utilizando el template "template.yaml"
 Variables a definir:

| Variable | Valor (Ejemplo) |
| ------ | ------ |
| BUCKET | develop-deploy-cloudformation-nolca|
| REGION | us-east-1 |
| AWS_PROFILE | nolca-nonprod |
| STACK | NOLCA-develop-WEB-instance |
| PROJECT | NOLCA |
| ENVIRONMENT | develop |
| SUBNETID | subnet-06ef661707e5fe90e |
| SECURITYGROUPID | sg-0a0157367c7063eb2|
| KEYNAME | nolcaweb-dev-ebs |
| INSTANCETYPE | t3.small |
| EBS | 50 |

Para la ejecución de los siguientes comando posicionarse en el directorio donde se encuentra el archivo template.yaml.
1- Crear bucket:
```sh
aws s3api create-bucket --bucket develop-deploy-cloudformation-nolca --region us-east-1 --profile nolca-nonprod
```
2-Validar SAM template local
```sh
sam validate --profile nolca-nonprod --region=us-east-1 --template "template.yaml"
```
3-Construir SAM local App
```sh
sam build --profile nolca-nonprod -t "template.yaml"
```
4-Empaquetar
```sh
sam package --profile nolca-nonprod --region=us-east-1 --template-file .aws-sam/build/template.yaml --output-template-file .aws-sam/build/packaged-template.yaml --s3-bucket develop-deploy-cloudformation-nolca
```
5-Construir SAM cloudformation
```sh
sam deploy --region us-east-1 --profile nolca-nonprod --region=us-east-1 --template-file .aws-sam/build/packaged-template.yaml --stack-name NOLCA-develop-WEB-instance --tags Project=NOLCA Environment=develop --parameter-overrides PublicSubnet=subnet-03da657ec17f8b321 Ec2SecurityGroup=sg-0a0157367c7063eb2 KeyName=nolcaweb-dev-ebs Customer=NOLCA InstanceType=t3.small Project=NOLCA EBS=50 --capabilities CAPABILITY_NAMED_IAM
```
> Nota: Tener en cuenta que la subnet tiene que pertenecer al security group

### Instalar Tomcat9
#### 1-Instalar Java 8
Ejecutar los siguientes comandos:
```sh
sudo amazon-linux-extras enable corretto8
```
```sh
sudo yum install java-1.8.0-amazon-corretto-devel
```
Ejecutar el siguiente comando para validar que se instalo correctamente:
```sh
java -version
```

### 2-Crear usuario y grupo de tomcat
Creamos un usuario y un grupo "tomcat"
```sh
sudo groupadd --system tomcat
```
```sh
sudo useradd -d /usr/share/tomcat -r -s /bin/false -g tomcat tomcat
```
Validamos la creación
```sh
getent passwd tomcat 
```
```sh
getent group tomcat 
```
### 3-Instalar Tomcat9
```sh
sudo yum -y install wget
```
```sh
export VER="9.0.39"
```
```sh
wget https://archive.apache.org/dist/tomcat/tomcat-9/v${VER}/bin/apache-tomcat-${VER}.tar.gz
```
```sh
sudo tar xvf apache-tomcat-${VER}.tar.gz -C /usr/share/
```
```sh
sudo ln -s /usr/share/apache-tomcat-$VER/ /usr/share/tomcat
```
Actualizamos los permisos
```sh
sudo chown -R tomcat:tomcat /usr/share/tomcat
```
```sh
sudo chown -R tomcat:tomcat /usr/share/apache-tomcat-$VER/ 
```
Creamos el servicio Tomcat Systemd
```sh
sudo tee /etc/systemd/system/tomcat.service<<EOF
[Unit]
Description=Tomcat Server
After=syslog.target network.target

[Service]
Type=forking
User=tomcat
Group=tomcat

Environment=CONFIG_HOME=/usr/share/tomcat/webapps/configuracion
Environment=JAVA_HOME=/usr/lib/jvm/jre
Environment='JAVA_OPTS=-Djava.awt.headless=true'
Environment=CATALINA_HOME=/usr/share/tomcat
Environment=CATALINA_BASE=/usr/share/tomcat
Environment=CATALINA_PID=/usr/share/tomcat/temp/tomcat.pid
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M'
ExecStart=/usr/share/tomcat/bin/catalina.sh start
ExecStop=/usr/share/tomcat/bin/catalina.sh stop

[Install]
WantedBy=multi-user.target
EOF
```
Habilitamos e iniciamos el servicio
```sh
sudo systemctl daemon-reload
```
```sh
sudo systemctl start tomcat
```
```sh
sudo systemctl enable tomcat
```

Validamos que tomcat se esta ejecutando
```sh
$ systemctl status tomcat
● tomcat.service - Tomcat Server
   Loaded: loaded (/etc/systemd/system/tomcat.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2020-10-10 11:18:40 UTC; 49s ago
 Main PID: 30574 (java)
   CGroup: /system.slice/tomcat.service
           └─30574 /usr/lib/jvm/jre/bin/java -Djava.util.logging.config.file=/usr/share/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.j...

Oct 10 11:18:40 ip-172-31-39-10.eu-west-1.compute.internal systemd[1]: Starting Tomcat Server...
Oct 10 11:18:40 ip-172-31-39-10.eu-west-1.compute.internal systemd[1]: Started Tomcat Server.
```
### 4-Configurar Autenticación de Tomcat
```shcd /usr
sudo vim /usr/share/tomcat/conf/tomcat-users.xml
```
Agregar las siguientes lineas antes que cierre el tag </tomcat-users>
```sh
<role rolename="admin-gui"/>
<role rolename="manager-gui"/>
<user username="admin" password="Naranja2021" fullName="Administrator" roles="admin-gui,manager-gui"/>
```
### 5-Configurar Apache web server
```sh
sudo yum -y install httpd 
```

Crear un archivo VirtualHost para la interfaz web de Tomcat Admin
```sh
$ sudo vim /etc/httpd/conf/http.conf
<VirtualHost *:80>
    ServerAdmin root@localhost
    ServerName ec2-54-81-21-163.compute-1.amazonaws.com
    DefaultType text/html
    ProxyRequests off
    ProxyPreserveHost On
    ProxyPass / http://localhost:8080/
    ProxyPassReverse / http://localhost:8080/
</VirtualHost>
```
Donde "ServerName" es el DNS donde esta tomcat server.
Reiniciamos httpd service:
```sh
sudo systemctl restart httpd
```
```sh
sudo systemctl enable httpd
```

### 6-Accedemos a la interfaz Tomcat para validar que funciona exitosamente
Abrir un navegador y escribir el nombre del DNS configurado en Apache para Apache Tomcat

### Agregar conecciones a las Base de Datos
Abrir el archivo "context.xml" que se encuentra en el directorio /usr/share/tomcat/conf y agregar las siguientes conecciones:

```sh
<Resource auth="Container" driverClassName="oracle.jdbc.driver.OracleDriver" maxActive="8" maxIdle="1" maxWait="10000" name="jdbc/bpmDS" password="bpm" type="javax.sql.DataSource" url="jdbc:oracle:thin:@dbtest01.naranja.com.ar:1521/tnproXA.naranja.com.ar" username="bpm"/>
<Resource auth="Container" driverClassName="oracle.jdbc.driver.OracleDriver" maxActive="8" maxIdle="1" maxWait="10000" name="jdbc/excelDS" password="proc321" type="javax.sql.DataSource" url="jdbc:oracle:thin:@cluster-homaix2:1521/liqcadbhom.naranja.com.ar" username="usr_proc"/>
<Resource auth="Container" driverClassName="oracle.jdbc.driver.OracleDriver" maxActive="8" maxIdle="1" maxWait="10000" name="jdbc/promocionesDS" password="Hola123" type="javax.sql.DataSource" url="jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dbdesa01)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=db01desa)))" username="admprom"/>
<Resource auth="Container" driverClassName="oracle.jdbc.driver.OracleDriver" maxActive="15" maxIdle="1" maxWait="10000000" name="jdbc/logueoDS" password="lognol22" type="javax.sql.DataSource" url="jdbc:oracle:thin:@//oratest01:1521/liqcadbtest" username="usr_log"/>
<Resource auth="Container" driverClassName="oracle.jdbc.driver.OracleDriver" maxActive="8" maxIdle="1" maxWait="10000" name="jdbc/pdfDS" password="liq123" type="javax.sql.DataSource" url="jdbc:oracle:thin:@192.168.222.53:1521/liqcadbtest" username="usr_liq"/>
```

## Crear bucket en AmazonS3 y subir archivo war y properties utilizados por nolca.
En el caso que sea necesario crear un bucket leer la el siguiente link:
                https://docs.aws.amazon.com/es_es/redshift/latest/dg/tutorial-loading-data-upload-files.html

Si el bucket ya existe, en el caso de nolca tenemos el bucket "deploy-nolca-web", actualizamos los archivos necesarios para el deploy de la aplicación.

### Descargar archivos de AmazonS3 a la instancia EC2
Descargamos el archivo .war y lo movemos /usr/share/tomcat/webapps/ al directorio ejecutando:
```sh
aws s3 cp s3://deploy-nolca-web/nolca-web-1.0.1.war /usr/share
```
```sh
mv nolca-web-1.0.1.war /usr/share/tomcat/webapps/
```
Creamos en el directorio "/usr/share/tomcat/webapps/" un directorio  llamado "configuracion":
```sh
cd /usr/share/tomcat/webapps/
```
```sh
mkdir configuracion
```
```sh
chmod 777 configuracion
```
Creamos en el directorio "/usr/share/tomcat/webapps/configuracion" los directorios "log" y "nolca":
```sh
cd /usr/share/tomcat/webapps/configuracion
```
```sh
mkdir nolca
```
```sh
chmod 777 nolca
```
```sh
mkdir log
```
```sh
chmod 777 log
```

Descargamos los archivos .properties de AmazonS3 a la instancia EC2
```sh
aws s3 cp s3://deploy-nolca-web/configuracion/nolca/seguridad.properties /usr/share
```
```sh
aws s3 cp s3://deploy-nolca-web/configuracion/nolca/datos.properties /usr/share
```
```sh
aws s3 cp s3://deploy-nolca-web/configuracion/nolca/servicios.properties /usr/share
```
```sh
aws s3 cp s3://deploy-nolca-web/configuracion/log/log4j.properties /usr/share
```
```sh
mv seguridad.properties /usr/share/tomcat/webapps/configuracion/nolca
```
```sh
mv datos.properties /usr/share/tomcat/webapps/configuracion/nolca
```
```sh
mv servicios.properties /usr/share/tomcat/webapps/configuracion/nolca
```
```sh
mv log4j.properties /usr/share/tomcat/webapps/configuracion/log
```
